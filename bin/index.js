#!/usr/bin/env node
/* eslint-disable @typescript-eslint/no-var-requires */
import fs from 'fs';
import path from 'path';
import YAML from 'yaml';
// import configPackage from '../package.json';
import config64 from '../index.js';
// print process.argv
/*
process.argv.forEach(function (val, index, array) {
  console.log(index + ': ' + val);
});
*/
const to_display_name = function (var_name) {
  let display_name = var_name.split('_').map(word => {
    return word[0].toUpperCase() + word.slice(1);
  }).join(' ');
  return display_name;
};
// const is_md = file => /.md$/.test(file);
const getExtension = file => file.split('.').slice(-1)[0];
const remove_extension = file => file.split('.').slice(0, -1).join('.');

const convertFile = function (target_path, configName) {
  let lines = [];
  let ext = getExtension(target_path);
  let modifiedDate;
  let config;
  if (ext === 'yaml') {
    console.log('yaml', target_path);
    modifiedDate = fs.statSync(target_path).mtime.toISOString();
    config = YAML.parse(fs.readFileSync(target_path, 'utf-8'));
  } else if (ext === 'json') {
    modifiedDate = fs.statSync(target_path).mtime.toISOString();
    config = JSON.parse(fs.readFileSync(target_path, 'utf-8'));
  }
  let config64string = config64.encode(config, configName, modifiedDate);
  lines.push(`# ${configName}`);
  lines.push(`CONFIG64=${config64string}`);
  return lines;
};


const getVarList = function (base_path, sub_path) {
  let lines = [];
  sub_path = sub_path || '';
  const target_dir = path.join(base_path, sub_path);
  fs.readdirSync(target_dir)
    // .filter(is_md)
    .forEach(fileName => {
      // let ext = getExtension(fileName);
      // let name = remove_extension(fileName);
      let configName = sub_path + fileName;
      const target_path = path.join(base_path, sub_path, fileName);
      // const target_path_relative = path.join(sub_path, fileName);
      if (fs.statSync(target_path).isDirectory()) {
        lines.concat(getVarList(base_path, path.join(sub_path, fileName)));
      } else {
        lines.concat(convertFile(target_path, configName));
      }
    });
  return lines;
};


let source_dir = process.argv[2] || process.cwd() || false;
if (!fs.existsSync(source_dir)) {
  console.log(process.cwd(), source_dir);
  source_dir = path.join(process.cwd(), source_dir);
}
if (!fs.existsSync(source_dir)) {
  console.log('Can not find config file');
  process.exit(1);
}

console.log('config64');
// console.log('version:', configPackage.version);
console.log('---');
console.log('from:', source_dir);

let output_lines = [];
if (fs.statSync(source_dir).isDirectory()) {
  console.log('- reading source directory');
  output_lines = getVarList(source_dir);
} else {
  console.log('- reading source file');
  let sourceFile = source_dir;
  let fileName = sourceFile.split('/').slice(-1)[0];
  // let configName = remove_extension(fileName);
  let configName = fileName;
  output_lines = convertFile(sourceFile, configName);
  source_dir = source_dir.split('/').slice(0, -1).join('/');
}

const output_file_name = process.argv[3] || '.env_configs';
let output_path = path.join(source_dir, output_file_name);
// const gitignore_path = '.gitignore';

console.log('  to:', output_path);
console.log('---');

fs.writeFileSync(
  output_path,
  output_lines.join('\n'),
);

console.log('DONE');
// console.log(output_lines);