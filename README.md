# config64

```js
import config64 from 'config64';

let config = {
  name: 'server config',
  version: '2022-02-02',
  greeting: 'hello',
  database_username: 'admin',
  database_password: '12345',
}

let configString = config64.encode(config);

// SERVER_CONFIG=${configString}

let importedConfig = config64.decode(configString);
```