export default {
  encode(config, name, version) {
    let project = name || config.name || 'project_config';
    project = project.replace(/\s/g, '_');
    version = version || config.version || false;
    if (!version) {
      let now = new Date();
      let date = `${now.getFullYear()}-${String(now.getMonth() + 1).padStart(2, '0')}-${String(now.getDate()).padStart(2, '0')}`;
      version = date;
    }
    let configString = JSON.stringify(config);
    let configBuffer = new Buffer.from(configString, 'utf8');
    let base64string = configBuffer.toString('base64');
    // fs.writeFileSync('./.env_paige_config', 'PAIGE_CONFIG=' + configBase64);
    let configBase64 = `${project},${version},${base64string}`;
    return configBase64;
  },
  decode(configBase64) {
    let [project, version, base64string] = configBase64.split(',');
    let configString = Buffer.from(base64string, 'base64').toString('utf8');
    let config = JSON.parse(configString);
    return config;
  }
};