# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.3](https://gitlab.com/mechkit/config64/compare/v0.0.2...v0.0.3) (2022-04-20)


### Bug Fixes

* bin name ([461ee74](https://gitlab.com/mechkit/config64/commit/461ee74e5ce37638982218540b2bf36beeb16995))

### [0.0.2](https://gitlab.com/mechkit/config64/compare/v0.0.1...v0.0.2) (2022-01-25)


### Features

* added ititial bin file ([5ecf0dc](https://gitlab.com/mechkit/config64/commit/5ecf0dce2e88cdc3c4ceb2fa22166a0f66ab0783))
* allow bin script to target one config file ([ae4f710](https://gitlab.com/mechkit/config64/commit/ae4f7107e2d5007e97d2fd9be6b626ab3f66d2be))
* changed seperator ([42ed5e1](https://gitlab.com/mechkit/config64/commit/42ed5e1010add429c1039479561920fa27d9aff4))
* improved default date-version ([fb30d66](https://gitlab.com/mechkit/config64/commit/fb30d66e9adcf4e93f7292ca05272ae752139fd5))


### Bug Fixes

* bin name ([9cb8493](https://gitlab.com/mechkit/config64/commit/9cb8493b2e3dc451f7b6dbdca841895ffcdd900c))
* string assembly ([1bd05c1](https://gitlab.com/mechkit/config64/commit/1bd05c1c22d58dbf82b0898df4e612495c5cdeb0))

### 0.0.1 (2022-01-25)


### Features

* init from other project ([ccc3d6d](https://gitlab.com/mechkit/config64/commit/ccc3d6d10ca4cbe56e870aa29ea173afb8b311d8))
